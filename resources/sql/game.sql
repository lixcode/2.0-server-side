
CREATE TABLE `group`
(
 `group_id` integer NOT NULL ,
 `name`     varchar(45) NOT NULL ,

PRIMARY KEY (`group_id`)
);

CREATE TABLE `user`
(
 `user_id`   integer  NOT NULL ,
 `online`    bit      NOT NULL ,
 `join_time` datetime NOT NULL ,
 `lost_time` datetime NULL ,

FOREIGN KEY (`group_id`) REFERENCES `group` (`group_id`)
);

CREATE TABLE `group_user`
(
 `group_user_id` integer NOT NULL ,
 `group_id`      integer NOT NULL ,
 `user_id`       integer  NOT NULL ,

PRIMARY KEY (`group_user_id`),
FOREIGN KEY (`group_id`) REFERENCES `group` (`group_id`)
FOREIGN KEY (`user_id`)  REFERENCES `user` (`user_id`)
);
