
CREATE TABLE `user`
(
 `user_id`  integer     NOT NULL ,
 `phone`    varchar(45) NOT NULL ,
 `login`    varchar(45) NOT NULL ,
 `name`     varchar(90) NOT NULL ,
 `mail`     varchar(45) NOT NULL ,
 `password` varchar(45) NULL ,

PRIMARY KEY (`user_id`)
);

CREATE TABLE `game`
(
 `game_id` integer     NOT NULL ,
 `name`    varchar(45) NOT NULL ,
 `begin`   datetime    NOT NULL ,
 `end`     datetime    NOT NULL ,
 `user_id` integer     NOT NULL ,

PRIMARY KEY (`game_id`),
FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
);

CREATE TABLE `plugin`
(
 `plugin_id` integer     NOT NULL ,
 `folder`    varchar(45) NOT NULL ,
 `name`      varchar(45) NOT NULL ,
 `author`    integer     NOT NULL ,

PRIMARY KEY (`plugin_id`),
FOREIGN KEY (`author`) REFERENCES `user` (`user_id`)
);

CREATE TABLE `version`
(
 `version_id` integer NOT NULL ,
 `plugin_id`  integer NOT NULL ,
 `major`      integer NOT NULL ,
 `minor`      integer NOT NULL ,

PRIMARY KEY (`version_id`),
FOREIGN KEY (`plugin_id`) REFERENCES `plugin` (`plugin_id`)
);

CREATE TABLE `game_plugin`
(
 `game_plugin_version` integer NOT NULL ,
 `game_id`             integer NOT NULL ,
 `version_id`          integer NOT NULL ,
 `group_id`            integer NOT NULL ,

PRIMARY KEY (`game_plugin_version`),
FOREIGN KEY (`game_id`)    REFERENCES `game` (`game_id`),
FOREIGN KEY (`version_id`) REFERENCES `version` (`version_id`)
);

CREATE TABLE `game_user`
(
 `game_user_id` integer NOT NULL ,
 `game_id`      integer NOT NULL ,
 `user_id`      integer NOT NULL ,

PRIMARY KEY (`game_user_id`),
FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`),
FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
);


